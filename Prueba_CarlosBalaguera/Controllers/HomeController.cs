﻿using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Buffers.Text;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Prueba_CarlosBalaguera.Controllers
{
    public class HomeController : Controller
    {

        public JsonResult GetJson()
        {
            string url = "https://sigma-studios.s3-us-west-2.amazonaws.com/test/colombia.json";
            using (WebClient wc = new WebClient())
            {
                //Leo el json
                var json = wc.DownloadString(url);
                var jsondeserialized = Newtonsoft.Json.JsonConvert.DeserializeObject(json);
                return new JsonResult { Data = jsondeserialized.ToString(), JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }

        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(string departamento, string ciudad, string name, string email)
        {
            if (departamento.Length > 30)
            {
                return View();
            }
            if (ciudad.Length > 30)
            {
                return View();
            }
            //Creo la conexion
            MySqlConnection conexion = new MySqlConnection();
            //Cadena de conexion
            conexion.ConnectionString = "Server = 178.128.146.252; Database = admin_sigmatest; Uid = admin_sigmauser; Pwd = pfaDKIJyPF;";
            //Abro la conexion
            conexion.Open();
            //Creo el comando
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = conexion;
            cmd.CommandText = "INSERT INTO contacts(name,email,state,city) VALUES(@name,@email,@state,@city)";
            cmd.Prepare();
            cmd.Parameters.AddWithValue("@Name", name);
            cmd.Parameters.AddWithValue("@email", email);
            cmd.Parameters.AddWithValue("@state", departamento);
            cmd.Parameters.AddWithValue("@city", ciudad);
            try
            {
                cmd.ExecuteNonQuery();
                TempData["Aprobado"] = "True";
                conexion.Close();
                return RedirectToAction("Index", "Home");
            }
            catch
            {
                //Poner algo para decir que hay un error
                conexion.Close();
                TempData["Aprobado"] = "rechazado";
                return View();
            }


        }
    }
}